const express = require('express');
const app = express();
const http = require('http')
app.get('/', (req, res) => {
    res.send({ hello: "Heroku" })
})
app.get('/db', (req, res) => {
    res.send({ Hello: "Heroku Data" })
})
const PORT = process.env.PORT || 5000;
app.listen(PORT, function () {
    console.log(`App listening on port ${PORT}`);
});